import os
import pickle

import docker

client = docker.from_env()

#client.images.build(path=".", tag="etl/test:0.1")

std_context = {
    "dwh_uri": "c100-226.cloud.gwdg.de",
    "dwh_user": "test",
    "dwh_pass": "test1",
    "dwh_db": "test",
}

flex_context = {
    "table_name": "random"
}

etl_name = "some_testname"

comm_path = f"/var/etl_runner/comm/{etl_name}/comm"
os.makedirs(comm_path, exist_ok=True)
with open(os.path.join(comm_path, "input.pkl"), "wb+") as in_pickle:
    pickle.dump(flex_context, in_pickle)

try:
    etl_container = client.containers.get(etl_name)
except docker.errors.NotFound:
    vols = {
            comm_path: {
                "bind": "/var/etl_communication", 
                "mode": "rw" 
                }
            }
    env = {}
    for k, v in std_context.items():
        env[k.upper()] = v
    etl_container = client.containers.create(
            environment=env, 
            image="etl/test:0.1",
            name=etl_name, 
            volumes=vols,
    )

etl_container.start()
container_logs = etl_container.logs(follow=True)
print(container_logs)

with open(os.path.join(comm_path, "output.pkl"), "rb") as out_pickle:
    print(pickle.load(out_pickle))
