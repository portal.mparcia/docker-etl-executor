#!/usr/bin/env python
# coding: utf-8

# # MLAB HL7 transformation to Datawarehouse

# Import all needed python base packages

import argparse
import os
import random
import string
import pickle

import pandas as pd

# sql load
from sqlalchemy import create_engine

def start_etl(context):
    print(f"Starting with context: {context}")

    gibberish = pd.DataFrame(None)
    for i in range(25):
        print(f"Creating random dataset {i}")
        data = {
            "a_string": "".join(random.choices(string.ascii_letters, k=15)),
            "a_number": random.randint(0, 250),
            "a_float": random.random(),
        }

        series = pd.Series(data)
        series.name = f"row_{i}"
        gibberish = gibberish.append(series)

    print("Uploading gibberish data to database")

    engine = create_engine(
        f"postgresql://{context['dwh_user']}:{context['dwh_pass']}@{context['dwh_uri']}:5432/{context['dwh_db']}",
        connect_args={"connect_timeout": 5},
    )
    gibberish.to_sql(context["table_name"], engine, if_exists="replace")

    return { "created_rows": 25 }

context = {
    "dwh_uri": "192.168.2.110",
    "dwh_user": "testfalse",
    "dwh_pass": "test",
    "dwh_db": "test",
}

for c_key, c_val in context.items():
    context[c_key] = os.getenv(c_key.upper(), default=c_val)

with open("/var/etl_communication/input.pkl", "rb") as in_pickle:
    ctx = pickle.load(in_pickle)
for k, v in ctx.items():
    context[k] = v

out_msg = start_etl(context)

with open("/var/etl_communication/output.pkl", "wb+") as out_pickle:
    pickle.dump(out_msg, out_pickle)
